package no.hvl.multecore.common.hierarchy;

import java.util.Set;

public interface INode {
	
	public String getName();
	
	public INode getType();

	public Set<INode> getTransitiveTypes();

	public Set<INode> getTransitiveTypesPlusInherited();

	// Does not include supplementary attributes
	public Set<IAttribute> getAttributes(boolean includePotencyAccessible);

	// Does not include supplementary attributes
	public Set<IAttribute> getAttributesPlusInherited(boolean includePotencyAccessible);

	public Set<DeclaredAttribute> getDeclaredAttributes(boolean includePotencyAccessible);
	
	public Set<DeclaredAttribute> getDeclaredAttributesPlusInherited(boolean includePotencyAccessible);

	public Set<DeclaredAttribute> getSupplementaryDeclaredAttributes(boolean includePotencyAccessible);
	
	public Set<DeclaredAttribute> getSupplementaryDeclaredAttributesPlusInherited(boolean includePotencyAccessible);

	// Includes supplementary attributes
	public Set<DeclaredAttribute> getAllDeclaredAttributes(boolean includePotencyAccessible);

	// Includes supplementary attributes
	public Set<DeclaredAttribute> geAllDeclaredAttributesPlusInherited(boolean includePotencyAccessible);
	
	public Set<InstantiatedAttribute> getInstantiatedAttributes();
	
	public Set<InstantiatedAttribute> getInstantiatedAttributesPlusInherited();

	// Does not include supplementary attributes
	public DeclaredAttribute getDeclaredAttribute(String attributeName, boolean includePotencyAccessible);

	// Does not include supplementary attributes
	public DeclaredAttribute getDeclaredAttributePlusInherited(String attributeName, boolean includePotencyAccessible);

	public DeclaredAttribute getSupplementaryDeclaredAttribute(String attributeName, boolean includePotencyAccessible);
	
	public DeclaredAttribute getSupplementaryDeclaredAttributePlusInherited(String attributeName, boolean includePotencyAccessible);
	
	// Includes supplementary attributes
	public DeclaredAttribute getDeclaredAttributeFromAll(String attributeName, boolean includePotencyAccessible);
	
	// Includes supplementary attributes
	public DeclaredAttribute getDeclaredAttributeFromAllPlusInherited(String attributeName, boolean includePotencyAccessible);

	public InstantiatedAttribute getInstantiatedAttribute(String attributeName);
	
	public InstantiatedAttribute getInstantiatedAttributePlusInherited(String attributeName);

	public Set<INode> getSupplementaryTypes();
	
	public Set<INode> getSupplementaryTypesPlusInherited();
	
	public INode getSupplementaryType(String typeName);
	
	public INode getSupplementaryTypeInModel(IModel supplementaryModel, boolean includePotencyAccessible);

	public IModel getModel();

	public Potency getPotency();

	public Set<INode> getParentNodes();

	public Set<INode> getAllParentNodes();
	
}

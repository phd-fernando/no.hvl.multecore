package no.hvl.multecore.common.hierarchy;

import java.util.Set;

public interface IEdge {
	
	public String getName();

	public IEdge getType();

	public INode getSource();
	
	public INode getTarget();

	public int getLowerBound();

	public int getUpperBound();
	
	public boolean isContainment();

	public IModel getModel();

	public Potency getPotency();
	
	public Set<IEdge> getSupplementaryTypes();
	
	public IEdge getSupplementaryType(String typeName, String sourceName);
	
	public IEdge getSupplementaryTypeInModel(IModel supplementaryModel);

}

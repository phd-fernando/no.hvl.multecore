package no.hvl.multecore.editor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IProject;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EStringToStringMapEntryImpl;

import no.hvl.multecore.common.MultEcoreManager;
import no.hvl.multecore.common.hierarchy.IEdge;
import no.hvl.multecore.common.hierarchy.INode;
import no.hvl.multecore.common.registry.ModelRegistryEntry;

public class MyServices {

	public MyServices() {

	}

	public List<String> getSuppNodeTypes(EClass self, String nameSuppHierarchy, String nameSuppModel) {
		List<String> nameNodes = new ArrayList<String>();
		String defaultNode = "";
		nameNodes.add(defaultNode);
		if (nameSuppHierarchy.equals(no.hvl.multecore.common.Constants.PRIMITIVE_TYPES_HIERARCHY)) {
			nameNodes.addAll(Arrays.asList(MultEcoreManager.instance().getPrimitiveTypesNames()));
		} else {
			for (INode node : MultEcoreManager.instance().getMultilevelHierarchy(nameSuppHierarchy).getModelByName(nameSuppModel).getNodes()) {
				nameNodes.add(node.getName());
			}
		}
		return nameNodes;
	}	
	
//TODO Finish this, where one can select types from the model selected as supp, and all of the models above it.	
//	public List<String> getSuppNodeTypes(EClass self, String nameSuppHierarchy, String nameSuppModel) {
//		List<String> nameNodes = new ArrayList<String>();
//		String defaultNode = "";
//		nameNodes.add(defaultNode);
//		if (nameSuppHierarchy.equals(no.hvl.multecore.common.Constants.PRIMITIVE_TYPES_HIERARCHY)) {
//			nameNodes.addAll(Arrays.asList(MultEcoreManager.instance().getPrimitiveTypesNames()));
//		} else {
//			Set <INode> allCandidateTypes = new HashSet<INode>();
//			allCandidateTypes.addAll(MultEcoreManager.instance().getMultilevelHierarchy(nameSuppHierarchy).getModelByName(nameSuppModel).getNodes());
//			for (INode iNode : allCandidateTypes) {
//				Map<INode, Set<IEdge>> allAvailableTypesInModel = MultEcoreManager.instance().getMultilevelHierarchy(nameSuppHierarchy).allAvailableTypesInModel(iNode.getModel(), false);
//				for (Map.Entry<INode, Set<IEdge>> entry : allAvailableTypesInModel.entrySet()) {
//					if (!nameNodes.contains(entry.getKey().getName())){
//						nameNodes.add(entry.getKey().getName());
//					}
//				}
//			}
//			for (INode node : allCandidateTypes) {
//				if (!nameNodes.contains(node.getName())){
//					nameNodes.add(node.getName());
//				}
//			}
//		}
//		return nameNodes;
//	}
	
	public List<String> getSuppEdgeTypes(EReference self, String nameSuppHierarchy, String nameSuppModel) {
		List<String> nameEdges = new ArrayList<String>();
		String defaultEdge = "";
		nameEdges.add(defaultEdge);
		if (nameSuppHierarchy.equals(no.hvl.multecore.common.Constants.PRIMITIVE_TYPES_HIERARCHY)) {
			nameEdges.addAll(Arrays.asList(MultEcoreManager.instance().getPrimitiveTypesNames()));
		} else {
			for (IEdge edge : MultEcoreManager.instance().getMultilevelHierarchy(nameSuppHierarchy).getModelByName(nameSuppModel).getEdges()) {
				nameEdges.add(edge.getName());
			}
		}
		return nameEdges;
	}
	
	public String getTypesEdge(EReference self, EAnnotation  supplementaryTypesAnnotation) {
		String computedName = "";
		for (EAnnotation eAnnotation : self.getEAnnotations()) {
			if (eAnnotation.getSource().startsWith("type")){
				String[] splittedSource = eAnnotation.getSource().split("=");
				computedName+= splittedSource [1];
			}
		}

		if (null != supplementaryTypesAnnotation && null != supplementaryTypesAnnotation.getDetails()) {
			for (Map.Entry<String, String> supplementaryTypePair : supplementaryTypesAnnotation.getDetails().entrySet()) {
				if (!supplementaryTypePair.getValue().equals("")) {
					computedName+= "," + supplementaryTypePair.getValue();
				}
			}
		}
		return computedName;
	}		
	
	public boolean isCPNProject(EPackage self) {
		URI resourceEMFURI = URI.createURI(self.eResource().getURI().toPlatformString(true));
		String[] segments = resourceEMFURI.toString().split(no.hvl.multecore.core.Constants.URI_SEPARATOR_SERIALIZED);
		segments = Arrays.copyOfRange(segments, 2, segments.length);
		resourceEMFURI = URI.createHierarchicalURI(segments, null, null);

		ModelRegistryEntry entry = MultEcoreManager.instance().getModelRegistry().getEntry(resourceEMFURI);
		IProject project = entry.getMetamodelIResource().getProject();
		return MultEcoreManager.instance().getMultilevelHierarchy(project).isCPN();
	}
	
}
